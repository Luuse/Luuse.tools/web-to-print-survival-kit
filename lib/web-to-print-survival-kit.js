

function srcLoad() {

	async function getContent(src) {
		let data = await fetch( src	);
		return data.text()
	}

	let articles = document.querySelectorAll('[data-src-chapter]')

	articles.forEach(function(item, i ){
		let src = item.getAttribute('data-src-chapter')
		getContent(src).then(data => {
			item.innerHTML = data
		}) 
	})

}

srcLoad()
